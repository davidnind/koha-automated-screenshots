# Automated screenshots for Koha documentation

## About this project

The aim of this project is to automate the capture of as many screenshots as possible for the Koha manual, in as many languages as possible (https://koha-community.org).

At the moment it is a proof-of-concept to see whether this approach will work.

## Overview

### The tools used

TO DO

### How it works

TO DO

### Tutorial

TO DO

### Guide

TO DO

### To do

See the issue tracker, the initial priorities are to:

* Create a step-by-step tutorial on how to setup you computer, run the available scripts to create screenshots, and add new screenshots.
* Create a detailed guide on creating screenshots automatically for Koha's documentation.
* Work out how to create screenshots for tabbed elements in the staff client and OPAC.
* Add more screenshots.

## Licence

The Koha software and manual are licensed under the GPL v3 or later.

The scripts and content for this project are licensed under the GPL v3 or later.
