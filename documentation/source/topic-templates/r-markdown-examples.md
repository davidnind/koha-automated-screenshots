---
id: markdown-examples
author: David Nind
copyright: 2018
audience:
source:
keyword:
shortdesc: Examples of markdown formattin for Lightweight DITA.
---

# Topic title

The first paragraph becomes the short description.

## Section title

This is a section. Ideally use only one heading level.

## Steps for tasks

```
1.  Step 1.
2.  Step 2.
3.  Step 3.
```

1.  Step 1.
2.  Step 2.
3.  Step 3.

## Unordered list

```
*   An item.
*   Another item.
```

*   Item 1
*   Item 2

## Numbered list

1.  Numbered item.
2.  Numbered item.
3.  Numbered item.

```
1.  Numbered item.
2.  Numbered item.
3.  Numbered item.
```

## Images

![Image](link-tp-impage-file.extension)

## Example

This is an example.

```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```

## Quote

As Kanye West said:

> We're living the future so
> the present is our past.

## Definition list

```
Term
:   Definition.
```

Term
:   Definition.

## inline code

I think you should use an `<addr>` element here instead.


## Table

| First Header | Second Header | Third Header |
|:-------------|:--------------|:-------------|
| Content      | *Long Cell*   | Cell         |
| Content      | **Cell**      | Cell         |

| Heading | 2 | 2 |  |
|:--------|:--|:--|:-|
| Cell    | 2 | 2 |  |
| Cell    | 2 | 2 |  |


First Header | Second Header
------------ | -------------
Content from cell 1 | Content from cell 2
Content in the first column | Content in the second column

| First Header | Second Header | Third Header |
| ------------ | :-----------: | -----------: |
| Content      | *Long Cell*                 ||
| Content      | **Cell**      | Cell         |

~~this~~

It's very easy to make some words **bold** and other words *italic* with Markdown. You can even [link to Google!](http://google.com)

*This text will be italic*

**This text will be bold**

*You **can** combine them*

`code`

~~strikethrough~~

## Possible YAML header profile entries

```---
id: t-task-template
author:
  - Author One
  - Author Two
source: Source
publisher: Publisher
permissions: Permissions
audience: Audience
category: Category
keyword:
  - Keyword1
  - Keyword2
resourceid:
  - Resourceid1
  - Resourceid2
workflow: review
---
```
