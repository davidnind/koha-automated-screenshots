*** Settings ***
Documentation     Resource file with common variables and keywords for Koha screen captures.
Library           SeleniumLibrary
Library           SeleniumScreenshots

*** Variables ***
${BROWSER}        Firefox
${DELAY}          0
${USER}           userid
${VALID ADMIN USER}     koha  # replace with admin user for your installation
${VALID ADMIN PASSWORD}  koha  # replace with admin password for your installation
${URL SERVER OPAC}    http://127.0.0.1:8080  # replace with OPAC URL for your installation
${URL SERVER STAFF}   http://127.0.0.1:8081  # replace with staff client URL for your installation
${URL STAFF CLIENT}   ${URL SERVER STAFF}/cgi-bin/koha/mainpage.pl

*** Keywords ***
Open Browser To Staff Client Login Page
    Open Browser    ${URL SERVER STAFF}    ${BROWSER}
    Set Selenium Speed    ${DELAY}
    Staff Client Login Page Should Be Open

Staff Client Login Page Should Be Open
    Title Should Be    Koha › Log in to Koha

Go To Staff Client Login Page
    Go To    ${URL STAFF CLIENT}
    Staff Client Login Page Should Be Open

Input Username
    [Arguments]    ${username}
    Input Text    userid    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    password    ${password}

Submit Credentials
    Click Button    Login

Staff Client Main Page Should Be Open
    Location Should Be    ${URL STAFF CLIENT}
    Title Should Be    Koha staff client
