*** Settings ***
Documentation     Screenshots for Koha - about.
Resource          resource.robot

Suite Teardown   Close Browser

*** Keywords ***



*** Test Cases ***

Login To Staff Client
    Open Browser To Staff Client Login Page
    Input Username    ${VALID ADMIN USER}
    Input Password    ${VALID ADMIN PASSWORD}
    Submit Credentials
    Staff Client Main Page Should Be Open

# About Koha pages - server information, perl modules and system information

About Koha
    Go To   ${URL SERVER STAFF}/cgi-bin/koha/about.pl

    Capture and crop page screenshot   images-automated/about/about-koha-server-info.png
    ...    css=.main
 
    Click Link   id:ui-id-2   # Perl modules
    Capture and crop page screenshot   images-automated/about/about-koha-perl-modules.png
    ...   css=.main

   Click Link  id:ui-id-3     # System information
   Capture and crop page screenshot   images-automated/about/about-koha-system-information.png
   ...   css=.main
