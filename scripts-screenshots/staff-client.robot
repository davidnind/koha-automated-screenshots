*** Settings ***
Documentation     Screenshots for Koha - Staff client.
Resource          resource.robot

Suite Teardown   Close Browser

*** Keywords ***



*** Test Cases ***

# Login prompt for staff client with user name an password filled out

Login To Staff Client
    Open Browser To Staff Client Login Page
    Input Username    ${VALID ADMIN USER}
    Input Password    ${VALID ADMIN PASSWORD}
    Capture and crop page screenshot  images-automated/staff-client/staff-client-login-page.png
    ...    css=#login
    Submit Credentials
    Staff Client Main Page Should Be Open

Staff Client Main Page
    Capture and crop page screenshot   images-automated/staff-client/staff-client-main-page.png
    ...    css=.intranet-main
